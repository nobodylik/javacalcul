package com.company;


import java.io.*;
import java.util.logging.FileHandler;
import java.util.*;
import java.io.IOException;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;



public class Main
{

    public static final Logger logger = Logger.getGlobal();



    public static void main(String[] args) {
        // write your code here


        InputStreamReader reader = null;
        try {
            FileHandler fh;
            fh = new FileHandler("C:\\Users\\user\\source\\repos\\java-calc\\MyLogFile.log");
            logger.addHandler(fh);
            SimpleFormatter formatter = new SimpleFormatter();
            fh.setFormatter(formatter);
            logger.setUseParentHandlers(false); // отключает вывод в консоль

            MyCalc Calc = new MyCalc();
            TextToStrings MyConv = new TextToStrings();

            if (args.length == 0) {
                System.out.println("буду использовать стандартный поток ввода для чтения команд");
                MyConv.Init(new InputStreamReader(System.in));
                logger.info("START\nчитаем команды из стандартного потока ввода\n");
            }

            if (args.length == 1) {
                System.out.print("буду читать команды из файла");
                System.out.println(args[0]);
                reader = new InputStreamReader(new FileInputStream(args[0]));
                MyConv.Init(reader);
                logger.info("START\nчитаем команды из файла: "+ args[0]+"\n");
            }




            while(MyConv.ReadStream())
                Calc.doCommand(MyConv.GetNextString());





        } catch (IOException e) {
            System.err.println("Error while reading file: " + e.getLocalizedMessage());
        } finally {
            logger.info("FINISH");
            if (null != reader) {
                try {
                    reader.close();
                } catch (IOException e) {
                    e.printStackTrace(System.err);
                }
            }
        }

    }
}