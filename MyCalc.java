package com.company;
//import com.sun.java.util.jar.pack.Instruction;

import java.util.*;

public class MyCalc
{
    Stack<Double> MyStack = new Stack<>();
    TreeMap<String, Double > ConstMap = new TreeMap<String, Double>();

    TreeMap<String, MyFunction > Functions = new TreeMap<String, MyFunction>();


    public String[] MySplit(String str)
    {   String[] subStr;
        String delimeter = " "; // Разделитель
        subStr = str.split(delimeter); // Разделения строки str с помощью метода split()
        return subStr;
    }

    /*double ss(double x) throws MyCalcExep
    {
        if (x<0)
            return Math.sqrt(x);
        else
            throw new MyCalcExep("ww","gg");
    }*/

   MyCalc()
   {
       Functions.put("PUSH", new MyPush());
       Functions.put("POP", new MyPop());
       Functions.put("PRINTALL", new PrintStack());
       Functions.put("DEFINE", new MyDefine());
       Functions.put("PRINT", new MyPrintTop());
       Functions.put("+", new MyPlus());
       Functions.put("-", new MyMinus());
       Functions.put("*", new MyUmnoj());
       Functions.put("/", new MyDelen());
       Functions.put("SQRT", new MySqrt());
       Functions.put("#", new MyComment());

       Main.logger.info("Инициализировали калькултор\n");
   }

    public void doCommand(String s)
    {
        try {
            Main.logger.info("Считали строку: " + s+"\n");
            String[] substr = MySplit(s);
            if (!Functions.containsKey(substr[0]))
                throw new MyCalcExep("Неизвестная команда", substr[0]);
            {
               MyFunction f =  Functions.get(substr[0]);
               f.Doit(MyStack,ConstMap,substr);
            }
        }
        catch( MyCalcExep e)
        {
            System.out.printf( "ERROR in command %s: %s\n", e.commandName, e.errorMessage);
        }

    }



}
