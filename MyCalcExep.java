package com.company;

import java.security.PublicKey;

public class MyCalcExep extends Exception
{
    public String commandName;
    public String errorMessage;

    MyCalcExep(String s1, String s2)
    {
        commandName = s1;
        errorMessage = s2;
        Main.logger.warning(" in command "+s1+": "+s2+"\n");
    }
}
