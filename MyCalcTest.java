package com.company;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.logging.Logger;

import static org.junit.Assert.*;

public class MyCalcTest {

    MyCalc obj;
    @Before
    public void setUp() throws Exception
    {
        Logger.getGlobal().setUseParentHandlers(false);
        obj = new MyCalc();

    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void mySplit()
    {
        String s = "test message split my str";
        String str[] = obj.MySplit(s);
        String ref[] ={"test", "message", "split", "my", "str"};

        for(int i=0; i<str.length; i++)
            assertEquals("mysplit", str[i],ref[i]);
    }

    @Test
    public void PlusTest()
    {
        obj.doCommand("PUSH 10");
        obj.doCommand("PUSH 12");
        obj.doCommand("+");

       Double a = obj.MyStack.pop();
        assertEquals("plus", a,22.0, 0.0001);
    }

    @Test
    public void DefineTest()
    {
        obj.doCommand("DEFINE a 1112");
        obj.doCommand("PUSH a");

        Double a = obj.MyStack.pop();
        assertEquals("define", a,1112.0, 0.0001);
    }

    @Test
    public void MinusTest()
    {
        obj.doCommand("PUSH 10");
        obj.doCommand("PUSH 12");
        obj.doCommand("-");

        Double a = obj.MyStack.pop();
        assertEquals("minus", a,2.0, 0.0001);
    }

    @Test
    public void UmnojTest()
    {
        obj.doCommand("PUSH 10");
        obj.doCommand("PUSH 12");
        obj.doCommand("*");

        Double a = obj.MyStack.pop();
        assertEquals("umnoj", a,120.0, 0.0001);
    }

    @Test
    public void DelenTest()
    {
        obj.doCommand("PUSH 2");
        obj.doCommand("PUSH 1239");
        obj.doCommand("/");

        Double a = obj.MyStack.pop();
        assertEquals("delen", a,619.5, 0.0001);
    }

    @Test
    public void SqrtTest()
    {
        obj.doCommand("PUSH 625.000000001");
        obj.doCommand("SQRT");

        Double a = obj.MyStack.pop();
        assertEquals("sqrt", a,25.0, 0.0001);
    }

    @Test
    public void SqrtTestOtr()
    {
        obj.doCommand("PUSH -18");
        obj.doCommand("SQRT");

        int a = obj.MyStack.size();
        assertEquals("sqrt (-)", a,0);
    }

    @Test
    public void PushTest()
    {
        int a = obj.MyStack.size();
        obj.doCommand("PUSH 12");
        int b = obj.MyStack.size();

        assertEquals("push", a+1, b,0);
    }

    @Test
    public void PopTest()
    {
        obj.doCommand("PUSH 1");
        int a = obj.MyStack.size();
        obj.doCommand("POP");
        int b = obj.MyStack.size();

        assertEquals("pop", a-1, b,0);
    }

}