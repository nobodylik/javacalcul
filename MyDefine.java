package com.company;

import java.util.Stack;
import java.util.TreeMap;

public class MyDefine extends MyFunction
{

    public void Doit(Stack<Double> MyStack, TreeMap<String, Double> ConstMap , String[] commands) throws MyCalcExep
    {
        if(commands.length <=1)
            throw new MyCalcExep("DEFINE", "мало аргументов");

        ConstMap.put(commands[1], Double.parseDouble(commands[2]));

        Main.logger.info("Успешно установили константу " + commands[1]+ "="+ commands[2]+"\n");
    }

}
