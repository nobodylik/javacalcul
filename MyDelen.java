package com.company;

import java.util.Stack;
import java.util.TreeMap;
import java.util.logging.Level;

public class MyDelen extends MyFunction {

    public void Doit(Stack<Double> MyStack, TreeMap<String, Double> ConstMap, String[] commands) throws MyCalcExep {
        if (MyStack.size() < 2)
            throw new MyCalcExep("/", "мало аргументов");

        double a = MyStack.pop();
        double b = MyStack.pop();

        if(b==0)
            throw new MyCalcExep("/", "деление на 0");

        MyStack.push(a / b);
        Main.logger.log(Level.INFO,"Успешно: " + Double.toString(a)+"/"+Double.toString(b)+"\n");

    }
}