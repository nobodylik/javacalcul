package com.company;

import java.util.*;
import java.util.logging.Level;

public class MyPop extends MyFunction
    {
        public void Doit(Stack<Double> MyStack, TreeMap<String, Double> ConstMap , String[] commands) throws MyCalcExep
        {
            if(MyStack.isEmpty())
                throw new MyCalcExep("POP", "стек пустой");

           double a = MyStack.pop();
            Main.logger.log(Level.INFO, "Успешно извлекли "+a+"\n ");
        }

    }
