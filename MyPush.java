package com.company;

import java.util.Stack;
import java.util.TreeMap;
import java.util.logging.Level;

public class MyPush extends MyFunction
{

    public void Doit(Stack<Double> MyStack, TreeMap<String, Double> ConstMap , String[] commands) throws MyCalcExep
    {
        if(commands.length <=1)
            throw new MyCalcExep("PUSH", "мало аргументов");


        if(ConstMap.containsKey(commands[1]))
        {
            MyStack.push(ConstMap.get(commands[1]));
            Main.logger.log(Level.INFO, "Добавили элемент "+commands[1]+ " в стэк\n");
        }

        else
        {
            MyStack.push(new Double(commands[1]));
            Main.logger.log(Level.INFO, "Добавили элемент "+commands[1]+ " в стэк\n");
        }



    }

}
