package com.company;

import java.util.Stack;
import java.util.TreeMap;
import java.util.logging.Level;

public class MySqrt extends MyFunction {

    public void Doit(Stack<Double> MyStack, TreeMap<String, Double> ConstMap, String[] commands) throws MyCalcExep {

        if (MyStack.size() < 1)
            throw new MyCalcExep("SQRT", "мало аргументов");

        double a = MyStack.pop();

        if (a<0)
            throw new MyCalcExep("SQRT", "корень из отрицательного числа");

        MyStack.push(Math.sqrt(a));

        Main.logger.log(Level.INFO, "Успешно извлекли квадратный корень из "+a+"\n");
    }
}