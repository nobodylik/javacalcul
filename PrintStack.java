package com.company;

import java.util.Stack;
import java.util.TreeMap;
import java.util.logging.Level;

public class  PrintStack extends MyFunction
{
    public void Doit(Stack<Double> MyStack, TreeMap<String, Double> ConstMap , String[] commands) throws MyCalcExep
    {

        if(MyStack.isEmpty())
            throw new MyCalcExep("PRINT", "стек пустой");  //??

        System.out.print("STACK:");
        for(int i =0; i<MyStack.size(); i++)
            System.out.printf(" %.2f", MyStack.get(i));
        System.out.println(" ");

        Main.logger.log(Level.INFO, "Успешно распечатали весь стэк \n");
    }
}
