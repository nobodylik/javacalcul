package com.company;
import java.io.*;
import java.io.IOException;
import  java.util.*;
import java.util.logging.Level;

public class TextToStrings {


    protected InputStreamReader MyStream;
    protected String NextString;

    public void Init(InputStreamReader reader) {
        MyStream = reader;
    }

    public String GetNextString() {
        return NextString;
    }

    public boolean ReadStream(){
        try {

            int i;
            StringBuilder word = new StringBuilder();

            while((i = MyStream.read()) != -1)
            {
                char c = (char)i;
                if(c=='\n' || c=='\r')
                {
                    if (word.toString().length()>0)
                    {
                        NextString = word.toString();
                        word.setLength(0);
                        return true;
                    }
                }
                else
                    word.append(c);
            }

            if (word.toString().length()>0) {
                NextString = word.toString();
                word.setLength(0);
                return true;
            }




        } catch (IOException e) {
            Main.logger.log(Level.INFO, "Не удалось открыть файл ");
            System.err.println("Error while reading file: " + e.getLocalizedMessage());
        }
        return false;
    }
}
